import {Logger} from './logger'

const logger = new Logger({
  host: '127.0.0.1',
  port: 9000
});

(async () => {
  let k = 0

  while (true) {
    ++k
    logger.info(`test ${k}`)

    await (new Promise(resolve => setTimeout(resolve, 1000)))
  }
})()