'use strict'
import Vue from 'vue'
import axios from 'axios'

import LinkedBoxComponent from './modules/LinkedEntities'

const $ = Vue.prototype.$jquery = window.jQuery

export async function render () {

  const isLead = /\/leads\/detail/.test(window.location.pathname)

  if (!window.devioDemo) {
    window.devioDemo = {}
  }

  // Добавляем бокс с информацией в сделку
  if (isLead) {
    if (window.devioDemo.linkedInfo) {
      console.log(`destroy`)
      await window.devioDemo.linkedInfo.$destroy()
      window.devioDemo.linkedInfo = null

      const $exists = $('#devio_linked')

      if ($exists[0]) {
        $exists.empty().remove()
      }
    }

    const LinkedBoxId = 'devio_linked'

    $('.card-entity-form__main-fields').before($(`<div id="${LinkedBoxId}"></div>`))

    window.devioDemo.linkedInfo = new Vue({
      el: `#${LinkedBoxId}`,
      extends: LinkedBoxComponent,
      propsData: {
        id: LinkedBoxId
      }
    })
  }

  return true
}