const path = require('path')
const webpack = require('webpack')
const {VueLoaderPlugin} = require('vue-loader')
const CopyPlugin = require('copy-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')

import dotenv from 'dotenv'

const env = process.env.NODE_ENV || 'dev'
const context = path.resolve(__dirname, '..')
const configPath = __dirname

dotenv.config({
  path: path.resolve(__dirname, `../../.env`)
})

let webpackConfig = {

  mode: env === 'prod' ? 'production' : 'development',
  context,

  entry: [
    `${context}/src/main.js`
  ],

  output: {
    path: `${context}/dist`,
    publicPath: process.env.WEBPACK_PUBLIC_PATH,
    filename: 'bundle.js',
    chunkFilename: `chunks/[name].[contenthash:5].js`,
    library: '[name]',
    libraryTarget: 'umd'
  },

  module: {
    rules: [
      {
        test: /\.svg$/,
        loader: 'vue-svg-loader'
      },
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.scss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.sass$/,
        use: [
          'vue-style-loader',
          'css-loader',
          'sass-loader?indentedSyntax'
        ]
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          cssSourceMap: true,
          cacheBusting: true,
          loaders: {
            'scss': [
              'vue-style-loader',
              'css-loader',
              'sass-loader'
            ],
            'sass': [
              'vue-style-loader',
              'css-loader',
              'sass-loader?indentedSyntax'
            ]
          }
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          babelrc: true,
          extends: `${configPath}/.babelrc`
        }
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'url-loader',
        options: {
          limit: 8192,
          name: 'assets/img/[name].[ext]'
        }
      }
    ]
  },
  resolve: {
    alias: {
      '~': path.resolve(__dirname, '../src'),
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json'],
    modules: ['node_modules']
  },
  plugins: [
    new VueLoaderPlugin(),
    new webpack.EnvironmentPlugin(process.env),
    new CopyPlugin([
      {from: `${context}/src/static`, to: `${context}/dist/static`}
    ]),
    new webpack.ProvidePlugin({Vue: 'vue', jQuery: 'jquery', $: 'jquery'})
  ],
  devServer: {
    host: process.env.WEBPACK_DEV_SERVER_HOST,
    port: process.env.WEBPACK_DEV_SERVER_PORT,

    publicPath: process.env.WEBPACK_DEV_SERVER,

    hot: true,
    hotOnly: false,
    inline: true,

    https: true,
    headers: {
      'Access-Control-Allow-Origin': '*'
    },

    overlay: {
      warnings: false,
      errors: true
    },

    watchOptions: {
      poll: false
    },

    disableHostCheck: true
  },
  devtool: 'cheap-module-eval-source-map',
  performance: {
    hints: false
  }
}

if (env === 'prod') {
  webpackConfig.devtool = '#hidden-source-map'

  webpackConfig.optimization = {
    minimizer: [new TerserPlugin({
      sourceMap: true,
      terserOptions: {
        compress: {
          drop_console: true
        }
      }
    })],
    minimize: true
  }
} else {
  webpackConfig.plugins = (webpackConfig.plugins || []).concat([
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ])
}

export default webpackConfig
