define([`@@path`], (bundle) => {
  return function () {

    let self = this

    Object.assign(self, {
      active: false,
      callbacks: {
        init () {

          let settings = self.get_settings(),
            active = settings && settings.widget_active === 'Y'

          self.active = active

          if (active) {
            try {
              bundle.render && bundle.render.call(self)
            } catch (e) {

            }
          }

          return true

        },
        render () {
          try {
            if (self.active) {
              bundle.render && bundle.render.call(self)
            }
          } catch (e) {}

          return true
        },
        settings ($modal) {
          try {
            bundle.settings && bundle.settings.call(self, $modal)
          } catch (e) {
            console.trace(e)
          }
        },
        onSave (settings) {

          let active = settings.active === 'Y'
          self.active = active

          if (active) {
            bundle.init && bundle.init.call(self)
          }

          return true

        },
        destroy () {
          bundle.destroy && bundle.destroy.call(self)
        },
        bind_actions () {
          bundle.bindActions && bundle.bindActions.call(self)
          return true
        },
        advancedSettings () {
          bundle.advancedSettings && bundle.advancedSettings.call(self)
        },
        contacts: {
          selected () {
            bundle.contactsSelected && bundle.contactsSelected.call(self)
          }
        },
        leads: {
          selected () {
            bundle.leadsSelected && bundle.leadsSelected.call(self)
          }
        },
      }
    })

    return this

  }
})